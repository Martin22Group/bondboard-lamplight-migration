<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkFileCategoryTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_file_category', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('fileid');
			$table->integer('categoryid');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->unique(['fileid','categoryid'], 'fileid_categoryid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_file_category');
	}
}
