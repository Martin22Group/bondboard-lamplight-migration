<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListProjectshareBodypolicyTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('list_projectshare_bodypolicy', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('text', 100);
			$table->text('share_policy')->nullable();
			$table->integer('parent')->index('parent');
			$table->integer('order')->nullable();
			$table->boolean('visible')->default(1);
			$table->integer('projectid')->default(1);
			$table->boolean('fixed')->default(0);
			$table->boolean('typeuserorg')->nullable();
			$table->text('typeuser')->nullable();
			$table->integer('bodyroletype')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('list_projectshare_bodypolicy');
	}
}
