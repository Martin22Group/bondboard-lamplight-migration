<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBodyVideoTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('body_video', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bodyid')->nullable()->default(0);
			$table->integer('videoid')->nullable()->default(0);
			$table->dateTime('first_watched')->nullable();
			$table->dateTime('date_watched')->nullable();
			$table->integer('number_views')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('body_video');
	}
}
