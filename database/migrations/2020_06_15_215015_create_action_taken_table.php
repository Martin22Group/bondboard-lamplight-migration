<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionTakenTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('action_taken', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('target_class');
			$table->integer('target_id');
			$table->integer('action_id');
			$table->mediumText('changes');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->index([DB::raw('target_class(191)'),'target_id'], 'target');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('action_taken');
	}
}
