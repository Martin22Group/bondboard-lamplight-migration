<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkLibraryItemTagTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_library_item_tag', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('itemid');
			$table->integer('tagid');
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->index(['itemid','tagid'], 'itemid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_library_item_tag');
	}
}
