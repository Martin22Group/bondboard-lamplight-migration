<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBodySettingsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('body_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bodyid')->index('bodyid');
			$table->mediumText('mysignature');
			$table->string('mywidgets');
			$table->string('custompageWidgets')->nullable();
			$table->text('customPageWidgetSettings')->nullable();
			$table->integer('summaryGroupDataView')->nullable();
			$table->integer('relationsGroupDataView')->nullable();
			$table->string('css');
			$table->integer('projectSummaryGroupDataView')->nullable();
			$table->integer('defaultProject')->nullable()->default(0);
			$table->integer('showContextMenu')->nullable()->default(1);
			$table->integer('contextMenuCases')->nullable()->default(1);
			$table->text('profileTabOrder')->nullable();
			$table->text('defaultSelectedProfileTab')->nullable();
			$table->integer('splitTabs')->nullable()->default(0);
			$table->integer('bigsaving')->nullable()->default(0);
			$table->integer('outcomeStyle')->nullable()->default(3);
			$table->integer('timefidelity')->nullable()->default(1);
			$table->text('diary')->nullable();
			$table->tinyInteger('timeoutUseConfirm')->nullable();
			$table->tinyInteger('showAccesskeys')->default(0);
			$table->tinyInteger('splitFormsToTabs')->default(1);
			$table->text('listDateFrom');
			$table->text('listDateTo');
			$table->text('alwaysAddMeToWork');
			$table->integer('myusersGroupDataView')->nullable();
			$table->mediumText('profileHeaderGroupDataView')->nullable();
			$table->tinyInteger('profileHeaderGroupDataViewShowAll')->default(1);
			$table->tinyInteger('showStickyTitles')->nullable()->default(1);
			$table->tinyInteger('showStaffInitialsInTable')->nullable()->default(0);
			$table->mediumText('defaultFormValues')->nullable();
			$table->longText('tableColumnState')->nullable();
			$table->tinyInteger('showReferralAppointment')->default(1);
			$table->mediumText('mail')->nullable();
			$table->text('happenGroupDataView')->nullable();
			$table->boolean('openPopupsInWindow')->nullable()->default(1);
			$table->integer('useAutosave')->nullable()->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('body_settings');
	}
}
