<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectMailchimpSettingsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_mailchimp_settings', function(Blueprint $table)
		{
			$table->integer('projectid')->primary();
			$table->text('apikey')->nullable();
			$table->text('listid')->nullable();
			$table->text('listname')->nullable();
			$table->text('webhook')->nullable();
			$table->integer('webhook_id')->nullable();
			$table->dateTime('webhook_registerdate')->nullable();
			$table->text('workareaid')->nullable();
			$table->integer('commtypeid')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_mailchimp_settings');
	}
}
