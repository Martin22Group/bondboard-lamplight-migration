<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenOutcomeTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_outcome', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('listoutcomeid')->index('listoutcomeid');
			$table->decimal('score', 10);
			$table->mediumText('comment')->nullable();
			$table->unsignedTinyInteger('ourwork');
			$table->mediumText('cause')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by')->index('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_outcome');
	}
}
