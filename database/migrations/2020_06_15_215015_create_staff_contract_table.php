<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffContractTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff_contract', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bodyid');
			$table->dateTime('date_from');
			$table->dateTime('date_to')->nullable();
			$table->decimal('salary', 10, 4);
			$table->decimal('chargeout', 10, 4)->nullable();
			$table->double('hours', 5, 3);
			$table->string('job_title')->nullable();
			$table->double('annual_leave', 7, 3);
			$table->date('annual_leave_yearstarts')->nullable();
			$table->integer('contract_status_id')->nullable();
			$table->integer('visible')->default(1);
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->timestamp('date_removed')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('staff_contract');
	}
}
