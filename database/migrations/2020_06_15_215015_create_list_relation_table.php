<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListRelationTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('list_relation', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('text', 100);
			$table->string('inverse_text', 100);
			$table->integer('parent')->index('parent');
			$table->integer('order')->nullable();
			$table->boolean('visible')->default(1);
			$table->integer('projectid')->default(1);
			$table->boolean('fixed')->default(0);
			$table->boolean('use_in_linked_records');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('list_relation');
	}
}
