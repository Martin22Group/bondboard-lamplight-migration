<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkHappenCaseTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_happen_case', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('happenid');
			$table->integer('caseid');
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->index(['caseid','happenid'], 'caseidkey');
			$table->index(['happenid','caseid'], 'happenid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_happen_case');
	}
}
