<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanningMidTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planning_mid', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bodyid')->default(0);
			$table->date('date_from')->nullable();
			$table->date('date_to')->nullable();
			$table->text('overview')->nullable();
			$table->integer('added_by')->nullable();
			$table->timestamp('date_added')->nullable()->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planning_mid');
	}
}
