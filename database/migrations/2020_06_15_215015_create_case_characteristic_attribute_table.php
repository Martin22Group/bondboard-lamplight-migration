<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaseCharacteristicAttributeTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('case_characteristic_attribute', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('caseid');
			$table->integer('characteristicid');
			$table->integer('characteristicoptionid')->nullable();
			$table->mediumText('textvalue')->nullable();
			$table->double('numbervalue')->nullable();
			$table->dateTime('datevalue')->nullable();
			$table->integer('attrset')->nullable()->default(0);
			$table->integer('record_number')->default(0);
			$table->boolean('visible')->default(1);
			$table->integer('projectid')->default(0);
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->timestamp('date_removed')->default('0000-00-00 00:00:00');
			$table->integer('removed_by')->nullable();
			$table->index(['caseid','characteristicid','visible'], 'casefields');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('case_characteristic_attribute');
	}
}
