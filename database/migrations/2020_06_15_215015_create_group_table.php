<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 100)->default('');
			$table->text('description');
			$table->text('criteria')->nullable();
			$table->date('date_added')->default('0000-00-00');
			$table->string('added_by', 100)->default('0');
			$table->unsignedInteger('locked_by')->default(0);
			$table->mediumText('sqldef');
			$table->mediumText('autodesc');
			$table->integer('typeid')->default(0);
			$table->integer('typebody')->default(1);
			$table->boolean('moving_dates')->nullable()->default(0);
			$table->boolean('waiting_list')->nullable()->default(0);
			$table->unsignedMediumInteger('waiting_list_remove')->nullable()->default(0);
			$table->unsignedInteger('waiting_list_alert')->nullable()->default(0);
			$table->unsignedInteger('waiting_list_alert_bodyid')->nullable();
			$table->unsignedInteger('waiting_list_target')->nullable()->default(0);
			$table->integer('summary_members')->nullable();
			$table->integer('visible')->default(1);
			$table->integer('projectid')->default(0);
			$table->integer('projectbodyid_added_by')->default(0);
			$table->integer('order')->default(0);
			$table->integer('status')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group');
	}
}
