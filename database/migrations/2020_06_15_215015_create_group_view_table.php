<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupViewTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_view', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->string('description');
			$table->integer('locked_by')->default(0);
			$table->text('sqldef');
			$table->text('autodesc');
			$table->text('coldef');
			$table->integer('added_by');
			$table->timestamp('date_added')->default('0000-00-00 00:00:00');
			$table->boolean('visible')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group_view');
	}
}
