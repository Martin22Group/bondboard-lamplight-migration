<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkBodyFileTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_body_file', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('fileid');
			$table->integer('bodyid');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->unique(['fileid','bodyid'], 'fileid_bodyid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_body_file');
	}
}
