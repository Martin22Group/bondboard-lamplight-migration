<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesChargepolicyTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_chargepolicy', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('name')->nullable();
			$table->mediumText('rules')->nullable();
			$table->unsignedTinyInteger('visible')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_chargepolicy');
	}
}
