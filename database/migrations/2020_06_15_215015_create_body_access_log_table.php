<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBodyAccessLogTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('body_access_log', function(Blueprint $table)
		{
			$table->integer('bodyid')->default(0);
			$table->integer('operator_bodyid')->default(0);
			$table->timestamp('date_added')->useCurrent();
			$table->unique(['bodyid','operator_bodyid'], 'bodyid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('body_access_log');
	}
}
