<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminLanguageTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_language', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->char('lang', 10);
			$table->integer('projectid')->nullable()->default(0);
			$table->string('term');
			$table->string('translation');
			$table->index(['lang','projectid'], 'loader');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_language');
	}
}
