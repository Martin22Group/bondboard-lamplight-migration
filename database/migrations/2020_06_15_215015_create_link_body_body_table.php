<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkBodyBodyTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_body_body', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bodyid')->index('bodyid');
			$table->integer('bodyid2')->index('bodyid22');
			$table->integer('relationid')->nullable();
			$table->boolean('direction')->default(1);
			$table->boolean('default_contact')->default(0);
			$table->text('notes');
			$table->timestamp('date_from')->default('0000-00-00 00:00:00');
			$table->timestamp('date_to')->nullable();
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->index(['relationid','bodyid'], 'relationid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_body_body');
	}
}
