<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesChargeComponentTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_charge_component', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('bodyid')->default(0);
			$table->unsignedInteger('happenid')->default(0);
			$table->unsignedInteger('attendee_bodyid')->default(0);
			$table->double('amount', 6, 2)->default(0.00);
			$table->double('taxrate', 6, 2)->default(0.00);
			$table->double('tax', 6, 2)->default(0.00);
			$table->double('gross', 6, 2)->default(0.00);
			$table->mediumInteger('notes')->nullable();
			$table->unsignedInteger('invoice_id')->nullable();
			$table->unsignedTinyInteger('visible')->default(1);
			$table->timestamp('date_added')->useCurrent();
			$table->unsignedInteger('added_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_charge_component');
	}
}
