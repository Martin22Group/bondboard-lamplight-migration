<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatainCampaignTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('datain_campaign', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('title');
			$table->string('type', 50);
			$table->dateTime('date_from');
			$table->dateTime('date_to')->nullable();
			$table->text('params')->nullable();
			$table->tinyInteger('personalised')->nullable();
			$table->tinyInteger('repeatable')->nullable()->default(0);
			$table->text('intro_text')->nullable();
			$table->text('end_text')->nullable();
			$table->text('after_text')->nullable();
			$table->unsignedTinyInteger('css_style')->nullable()->default(1);
			$table->unsignedTinyInteger('css_colour')->nullable()->default(1);
			$table->unsignedTinyInteger('use_logo')->nullable()->default(1);
			$table->unsignedSmallInteger('numinvitees')->nullable()->default(0);
			$table->unsignedSmallInteger('numresponses')->nullable()->default(0);
			$table->unsignedSmallInteger('projectid')->nullable()->default(0);
			$table->integer('added_by')->nullable();
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('datain_campaign');
	}
}
