<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenGrantApplicationTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_grant_application', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('grant_reference', 50)->default('0');
			$table->integer('programmeid')->default(0);
			$table->dateTime('date_activity_from');
			$table->dateTime('date_activity_to');
			$table->decimal('requested_amount', 10, 0);
			$table->decimal('grant_amount', 10, 0);
			$table->decimal('total_amount', 10, 0);
			$table->integer('statusid');
			$table->text('summary')->nullable();
			$table->text('description')->nullable();
			$table->text('followup')->nullable();
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_grant_application');
	}
}
