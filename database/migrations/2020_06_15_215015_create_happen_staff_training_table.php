<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenStaffTrainingTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_staff_training', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('liststafftrainingtypeid')->index('liststafftrainingtypeid');
			$table->integer('liststafftraininglevelid')->index('liststafftraininglevelid');
			$table->dateTime('date_training_started');
			$table->dateTime('date_training_complete')->nullable();
			$table->mediumText('training_provider')->nullable();
			$table->string('description')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by')->index('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_staff_training');
	}
}
