<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListMessagerecipienttypeTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('list_messagerecipienttype', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('text', 100);
			$table->integer('parent')->index('parent');
			$table->integer('order')->nullable();
			$table->boolean('visible')->default(1);
			$table->integer('projectid')->default(1);
			$table->boolean('fixed')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('list_messagerecipienttype');
	}
}
