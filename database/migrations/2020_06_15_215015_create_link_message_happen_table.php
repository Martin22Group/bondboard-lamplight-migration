<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkMessageHappenTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_message_happen', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('messageid')->index('messageid');
			$table->integer('happenid')->index('happenid');
			$table->integer('typeid');
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->boolean('visible');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_message_happen');
	}
}
