<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendCharacteristicTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attend_characteristic', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('typework')->nullable();
			$table->boolean('typeoutcome')->nullable();
			$table->boolean('typecomm')->nullable();
			$table->boolean('typecharge')->nullable();
			$table->boolean('typeeval')->nullable();
			$table->boolean('typereferral')->nullable();
			$table->boolean('typegrant')->nullable();
			$table->string('text');
			$table->integer('characteristictype')->default(1)->comment('1=select; 2=multi-select; 3=text, 4=textarea, 5=date');
			$table->boolean('required')->default(0);
			$table->integer('panel')->nullable();
			$table->boolean('historical')->default(0);
			$table->integer('record_id')->index('record_id');
			$table->integer('order');
			$table->boolean('access_manager')->default(0);
			$table->unsignedTinyInteger('share_role')->default(0);
			$table->unsignedSmallInteger('share_rights')->default(0);
			$table->boolean('publishable')->default(0);
			$table->boolean('alterable')->default(0);
			$table->boolean('visible');
			$table->boolean('fixed')->default(0);
			$table->text('css_style')->nullable();
			$table->text('description_text')->nullable();
			$table->text('placeholder_text')->nullable();
			$table->text('validator')->nullable();
			$table->text('default_value')->nullable();
			$table->text('javascript')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->timestamp('date_removed')->nullable();
			$table->text('group_data_view_columnref')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attend_characteristic');
	}
}
