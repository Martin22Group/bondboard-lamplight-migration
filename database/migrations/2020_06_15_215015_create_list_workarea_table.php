<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListWorkareaTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('list_workarea', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('text', 100);
			$table->integer('parent')->index('parent');
			$table->integer('order')->nullable();
			$table->boolean('visible')->default(1);
			$table->integer('projectid')->default(1);
			$table->boolean('fixed')->default(0);
			$table->integer('typeid')->default(0);
			$table->char('back_colour', 6)->nullable()->default('FFFFFF');
			$table->char('color', 6)->nullable()->default('000000');
			$table->char('back_color', 6)->nullable()->default('FFFFFF');
			$table->index(['visible','id'], 'visible');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('list_workarea');
	}
}
