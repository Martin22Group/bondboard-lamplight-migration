<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceBodyExternallinkTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_body_externallink', function(Blueprint $table)
		{
			$table->integer('bodyid');
			$table->integer('typeid');
			$table->text('externalid');
			$table->primary(['bodyid','typeid']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_body_externallink');
	}
}
