<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('search', function(Blueprint $table)
		{
			$table->integer('id')->default(0);
			$table->integer('projectid')->default(0)->index('projectid');
			$table->text('class')->nullable();
			$table->dateTime('record_date')->nullable();
			$table->integer('type')->nullable();
			$table->integer('key')->nullable();
			$table->text('name')->nullable();
			$table->text('summary')->nullable();
			$table->text('details')->nullable();
			$table->text('keywords')->nullable();
			$table->text('publish_details')->nullable();
			$table->boolean('publishable')->nullable()->default(0);
			$table->unsignedInteger('added_by')->nullable()->default(0);
			$table->timestamp('date_added')->useCurrent();
			$table->index([DB::raw('details(191)'),DB::raw('summary(191)'),DB::raw('name(191)')], 'details');
			$table->primary(['id','projectid']);
			$table->index([DB::raw('publish_details(191)'),DB::raw('summary(256)'),DB::raw('name(191)')], 'publish_details');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('search');
	}
}
