<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('typeid')->nullable();
			$table->integer('linkid')->nullable();
			$table->integer('happen_char_tabid');
			$table->integer('numbodys');
			$table->integer('numusers')->default(0);
			$table->integer('maxnumusers');
			$table->dateTime('date_from')->nullable()->index('date_from');
			$table->dateTime('date_to')->nullable()->index('date_to');
			$table->integer('workareaid')->nullable();
			$table->decimal('session_cost', 8, 4)->nullable()->default(0.0000);
			$table->boolean('visible')->default(1)->index('visible');
			$table->integer('projectid')->nullable();
			$table->boolean('publishable')->default(0);
			$table->boolean('datain')->default(0);
			$table->text('recur')->nullable();
			$table->integer('parent_id')->nullable();
			$table->integer('charge_policyid')->nullable();
			$table->integer('payee_policyid')->nullable();
			$table->tinyInteger('showOnDiary')->default(1);
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->integer('created_by');
			$table->timestamp('date_created')->default('0000-00-00 00:00:00');
			$table->index(['visible','projectid','typeid','date_from','id','workareaid'], 'typeid');
			$table->index(['typeid','linkid','visible','date_from','projectid','id'], 'typeLinkBody');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen');
	}
}
