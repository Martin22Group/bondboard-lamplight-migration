<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListLocationTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('list_location', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('text', 100);
			$table->integer('parent')->index('parent');
			$table->integer('order')->nullable();
			$table->boolean('visible')->default(1);
			$table->integer('projectid')->default(1);
			$table->boolean('fixed')->default(1);
			$table->char('color', 6)->nullable()->default('000000');
			$table->char('back_colour', 6)->nullable()->default('FFFFFF');
			$table->string('address_line_1', 100)->nullable();
			$table->string('address_line_2', 100)->nullable();
			$table->string('address_line_3', 100)->nullable();
			$table->string('address_line_4', 100)->nullable();
			$table->string('address_line_5', 100)->nullable();
			$table->string('postcode', 20)->nullable();
			$table->string('borough', 100)->nullable();
			$table->string('ward', 100)->nullable();
			$table->string('contact_first_name', 40)->nullable();
			$table->string('contact_surname', 40)->nullable();
			$table->string('contact_email', 50)->nullable();
			$table->string('contact_phone', 20)->nullable();
			$table->text('description')->nullable();
			$table->string('website')->nullable();
			$table->text('disabled_access')->nullable();
			$table->decimal('hourly_rate', 10)->nullable();
			$table->decimal('chargeout', 10)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('list_location');
	}
}
