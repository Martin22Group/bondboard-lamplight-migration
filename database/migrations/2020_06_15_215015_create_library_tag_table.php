<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibraryTagTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('library_tag', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('text')->nullable();
			$table->integer('visible')->nullable()->default(1);
			$table->integer('added_by')->nullable();
			$table->timestamp('date_added')->nullable()->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('library_tag');
	}
}
