<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkHappenFileTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_happen_file', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('happenid');
			$table->integer('fileid');
			$table->index(['happenid','fileid'], 'happenid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_happen_file');
	}
}
