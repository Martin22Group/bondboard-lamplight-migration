<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenStaffAppraisalTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_staff_appraisal', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('liststaffappraisaltypeid')->index('liststaffappraisaltypeid');
			$table->integer('liststaffappraisaloutcomeid')->index('liststaffappraisaloutcomeid');
			$table->mediumText('staff_comment')->nullable();
			$table->mediumText('manager_comment')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by')->index('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_staff_appraisal');
	}
}
