<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommTemplateTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comm_template', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('template_name');
			$table->text('short_desc')->nullable();
			$table->tinyInteger('commsrole_combine')->nullable();
			$table->text('template_body');
			$table->string('template_file')->nullable()->default('');
			$table->boolean('visible')->default(1);
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comm_template');
	}
}
