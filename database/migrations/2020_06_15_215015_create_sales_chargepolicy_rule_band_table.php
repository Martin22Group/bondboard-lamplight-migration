<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesChargepolicyRuleBandTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_chargepolicy_rule_band', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('name');
			$table->integer('groupid')->nullable();
			$table->text('rate');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_chargepolicy_rule_band');
	}
}
