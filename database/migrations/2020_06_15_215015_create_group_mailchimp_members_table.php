<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupMailchimpMembersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_mailchimp_members', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('groupid')->nullable()->default(0)->index('group_id');
			$table->integer('bodyid')->default(0)->index('bodyid');
			$table->integer('bodyid2')->nullable()->default(0);
			$table->string('email')->index('email');
			$table->text('mcemail_id')->nullable();
			$table->boolean('which_address')->default(1);
			$table->tinyInteger('status')->nullable()->default(0);
			$table->timestamp('sync')->nullable();
			$table->unique(['groupid', DB::raw('email(100)'),'status'], 'groupid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group_mailchimp_members');
	}
}
