<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListGrantProgrammeTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('list_grant_programme', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('text');
			$table->dateTime('apply_date_from');
			$table->dateTime('apply_date_to');
			$table->decimal('total_value', 10);
			$table->text('programme_aims');
			$table->text('programme_eligibility');
			$table->text('programme_requirements');
			$table->tinyInteger('open')->default(1);
			$table->tinyInteger('visible')->default(1);
			$table->smallInteger('order')->default(1);
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('list_grant_programme');
	}
}
