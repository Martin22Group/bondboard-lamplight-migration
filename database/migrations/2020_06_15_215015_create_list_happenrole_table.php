<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListHappenroleTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('list_happenrole', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('text', 100);
			$table->integer('parent')->index('parent');
			$table->integer('order')->nullable();
			$table->boolean('visible')->default(1);
			$table->integer('projectid')->default(1);
			$table->boolean('fixed')->default(1);
			$table->integer('typeid');
			$table->integer('default_profile_type')->nullable()->default(0);
			$table->integer('default_profile_role')->nullable()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('list_happenrole');
	}
}
