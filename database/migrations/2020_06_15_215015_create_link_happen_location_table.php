<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkHappenLocationTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_happen_location', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('happenid');
			$table->integer('locationid')->index('workareaid');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->index(['happenid','locationid'], 'linkid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_happen_location');
	}
}
