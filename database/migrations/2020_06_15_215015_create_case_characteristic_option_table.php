<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaseCharacteristicOptionTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('case_characteristic_option', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('characteristicid');
			$table->string('text');
			$table->integer('order');
			$table->boolean('visible')->default(1);
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('case_characteristic_option');
	}
}
