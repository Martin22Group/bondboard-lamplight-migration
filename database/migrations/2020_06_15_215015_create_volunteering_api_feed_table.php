<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVolunteeringApiFeedTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('volunteering_api_feed', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->char('feed_key', 4)->nullable();
			$table->string('feed_name')->nullable();
			$table->string('postcode', 12)->nullable();
			$table->integer('distance')->nullable();
			$table->tinyInteger('include_form')->nullable();
			$table->text('topics')->nullable();
			$table->text('roles')->nullable();
			$table->text('custom_css')->nullable();
			$table->integer('num_per_page')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('volunteering_api_feed');
	}
}
