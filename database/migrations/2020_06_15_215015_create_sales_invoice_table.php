<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesInvoiceTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_invoice', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('invoice_date');
			$table->integer('bodyid');
			$table->mediumText('description')->nullable();
			$table->text('po_number')->nullable();
			$table->date('due_date');
			$table->text('invoice_number')->nullable();
			$table->decimal('net_amount', 10)->default(0.00);
			$table->decimal('tax_amount', 10)->default(0.00);
			$table->decimal('gross_amount', 10)->default(0.00);
			$table->integer('export_fileid')->nullable();
			$table->integer('projectid');
			$table->timestamp('date_added')->useCurrent();
			$table->unsignedInteger('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_invoice');
	}
}
