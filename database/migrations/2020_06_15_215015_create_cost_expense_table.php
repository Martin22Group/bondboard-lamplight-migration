<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCostExpenseTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cost_expense', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bodyid');
			$table->integer('happenid')->nullable();
			$table->dateTime('expense_date');
			$table->decimal('quantity', 10)->nullable();
			$table->decimal('amount', 10);
			$table->integer('categoryid')->nullable();
			$table->integer('statusid')->nullable();
			$table->string('details', 1000)->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by')->default(0);
			$table->integer('authorised_by')->nullable();
			$table->timestamp('date_authorised')->nullable();
			$table->integer('paid_by')->nullable();
			$table->timestamp('date_paid')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cost_expense');
	}
}
