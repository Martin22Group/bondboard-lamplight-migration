<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkStaffStaffTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_staff_staff', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('juniorbodyid')->index('juniorbodyid');
			$table->integer('seniorbodyid')->index('seniorbodyid');
			$table->timestamp('date_from')->default('0000-00-00 00:00:00');
			$table->timestamp('date_to')->nullable();
			$table->integer('projectid');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_staff_staff');
	}
}
