<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminOutcomestarOperatorsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_outcomestar_operators', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bodyid')->nullable();
			$table->integer('projectid')->nullable();
			$table->text('star_ids')->nullable();
			$table->timestamp('date_added')->nullable();
			$table->integer('added_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_outcomestar_operators');
	}
}
