<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('file', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('useType')->comment('1 = library, 2 = user generated, 7 = multi media module');
			$table->enum('mime_main_type', ['1','2','3','4','5','6','7','8','9'])->comment('1 = app, 2 = audio, 3 = example, 4 = img, 5 =message, 6 = model, 7 = multipart, 8 = text, 9 = video');
			$table->string('reference_name', 100);
			$table->string('filename');
			$table->string('location');
			$table->integer('filesize');
			$table->text('mimetype');
			$table->text('meta');
			$table->text('externalurl');
			$table->text('description');
			$table->mediumText('preview')->nullable();
			$table->unsignedInteger('projectid')->nullable();
			$table->unsignedTinyInteger('index')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->boolean('visible')->default(1);
			$table->index(['useType','mime_main_type','filename'], 'byUseType');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('file');
	}
}
