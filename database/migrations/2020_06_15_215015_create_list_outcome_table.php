<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListOutcomeTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('list_outcome', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('text', 100);
			$table->integer('parent')->index('parent');
			$table->integer('order')->nullable();
			$table->boolean('visible')->default(1);
			$table->integer('projectid')->default(1);
			$table->boolean('fixed')->default(0);
			$table->integer('outcome_type')->comment('0=score, 1=yesno, 2=number');
			$table->text('notes');
			$table->integer('minValue')->nullable()->default(0);
			$table->integer('maxValue')->nullable()->default(10);
			$table->integer('defaultValue')->nullable();
			$table->integer('use_summary')->nullable();
			$table->text('formula')->nullable();
			$table->integer('formula_level')->nullable();
			$table->text('formula_args')->nullable();
			$table->integer('bodytype')->nullable();
			$table->integer('bodyroletype')->nullable();
			$table->integer('external_source')->nullable();
			$table->integer('external_id')->nullable();
			$table->boolean('show_scores_without_descriptors')->nullable()->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('list_outcome');
	}
}
