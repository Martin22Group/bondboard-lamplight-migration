<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCostLocationTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cost_location', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('locationid');
			$table->dateTime('date_from');
			$table->dateTime('date_to')->nullable();
			$table->decimal('hourly_cost', 10, 4);
			$table->integer('visible')->default(1);
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->timestamp('date_removed')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cost_location');
	}
}
