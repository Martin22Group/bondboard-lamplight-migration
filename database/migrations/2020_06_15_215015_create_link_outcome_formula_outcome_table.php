<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkOutcomeFormulaOutcomeTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_outcome_formula_outcome', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('dependent_outcome_id')->default(0);
			$table->integer('independent_outcome_id')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_outcome_formula_outcome');
	}
}
