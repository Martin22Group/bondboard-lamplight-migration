<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkBodyAddressTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_body_address', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bodyid')->index('bodyid');
			$table->integer('addressid')->index('addressid');
			$table->timestamp('date_from')->default('0000-00-00 00:00:00');
			$table->timestamp('date_to')->nullable()->default('0000-00-00 00:00:00');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_body_address');
	}
}
