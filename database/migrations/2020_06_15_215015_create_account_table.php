<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('account', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->double('balance', 6, 2);
			$table->double('change', 6, 2);
			$table->integer('type')->index('ty[e')->comment('1: credit; 2:sms; 3:postcode');
			$table->integer('bodyid');
			$table->integer('added_by');
			$table->dateTime('date_added');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('account');
	}
}
