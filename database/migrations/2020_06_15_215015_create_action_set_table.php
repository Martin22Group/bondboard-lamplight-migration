<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionSetTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('action_set', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('name');
			$table->text('action_ids');
			$table->tinyInteger('stop_on_failconstraint')->default(0);
			$table->text('target_class')->nullable();
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('action_set');
	}
}
