<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminFileinsertLogTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_fileinsert_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime('date_from')->nullable();
			$table->dateTime('date_to')->nullable();
			$table->text('detail')->nullable();
			$table->text('type')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->unsignedInteger('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_fileinsert_log');
	}
}
