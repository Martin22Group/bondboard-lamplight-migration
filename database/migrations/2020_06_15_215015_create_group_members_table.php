<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupMembersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_members', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('groupid');
			$table->integer('bodyid');
			$table->integer('bodyid2')->nullable()->default(0);
			$table->boolean('which_address')->default(1);
			$table->integer('member_status')->default(0);
			$table->dateTime('date_joined')->nullable();
			$table->dateTime('date_left')->nullable();
			$table->text('notes')->nullable();
			$table->integer('added_by')->nullable();
			$table->timestamp('date_added')->nullable()->useCurrent();
			$table->index(['bodyid2','groupid'], 'body2first');
			$table->index(['bodyid','groupid'], 'bodyfirst');
			$table->index(['groupid','bodyid','bodyid2'], 'groupfirst');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group_members');
	}
}
