<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkHappenBody2Table extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_happen_body2', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('happenid');
			$table->integer('bodyid');
			$table->integer('secondary_linkid')->nullable();
			$table->integer('attendanceid');
			$table->integer('roleid');
			$table->integer('commsroleid')->nullable();
			$table->string('notes')->nullable();
			$table->decimal('charge')->default(0.00);
			$table->unsignedInteger('charge_band')->nullable();
			$table->decimal('charge_tax')->nullable();
			$table->decimal('charge_gross')->default(0.00);
			$table->dateTime('date_booked')->nullable();
			$table->dateTime('date_signed_out')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->boolean('visible');
			$table->index(['bodyid','happenid'], 'bodyid');
			$table->index(['happenid','bodyid'], 'happenid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_happen_body2');
	}
}
