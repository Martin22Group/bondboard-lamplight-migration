<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkHappenWorkareaTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_happen_workarea', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('happenid')->index('linkid');
			$table->integer('workareaid');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->index(['workareaid','happenid'], 'workareaid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_happen_workarea');
	}
}
