<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanningHighTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planning_high', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('date_from')->nullable();
			$table->date('date_to')->nullable();
			$table->text('overview')->nullable();
			$table->integer('added_by')->nullable();
			$table->timestamp('date_added')->nullable()->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planning_high');
	}
}
