<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountBillingTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('account_billing', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('type');
			$table->mediumText('item')->nullable();
			$table->mediumText('description')->nullable();
			$table->dateTime('transaction_date')->nullable();
			$table->integer('units')->nullable();
			$table->double('value', 8, 2)->nullable();
			$table->integer('may_remove')->nullable();
			$table->integer('added_by');
			$table->dateTime('date_added');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('account_billing');
	}
}
