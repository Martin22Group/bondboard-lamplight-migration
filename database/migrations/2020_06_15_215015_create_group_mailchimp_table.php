<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupMailchimpTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_mailchimp', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('group_id')->default(0)->index('group_id');
			$table->dateTime('date_sync');
			$table->text('mc_segmentid')->nullable();
			$table->text('mc_interest_group_id')->nullable();
			$table->text('mc_parent_group_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group_mailchimp');
	}
}
