<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportSummaryTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_summary', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('date_from')->index('date_from');
			$table->integer('num_users')->default(0);
			$table->integer('num_staff')->default(0);
			$table->integer('num_vols')->default(0);
			$table->integer('num_other')->default(0);
			$table->integer('typeid');
			$table->integer('num_secs')->default(0);
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('report_summary');
	}
}
