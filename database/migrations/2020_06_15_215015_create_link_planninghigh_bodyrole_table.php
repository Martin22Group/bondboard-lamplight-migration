<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkPlanninghighBodyroleTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_planninghigh_bodyrole', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('planningid')->nullable();
			$table->integer('typeuser')->nullable();
			$table->integer('typestaff')->nullable();
			$table->integer('typecontact')->nullable();
			$table->integer('typefunder')->nullable();
			$table->integer('typeorg')->nullable();
			$table->integer('added_by')->nullable();
			$table->timestamp('date_added')->nullable()->useCurrent();
			$table->integer('bodyroletype')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_planninghigh_bodyrole');
	}
}
