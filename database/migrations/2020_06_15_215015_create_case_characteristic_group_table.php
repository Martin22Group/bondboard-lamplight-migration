<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaseCharacteristicGroupTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('case_characteristic_group', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('groupname');
			$table->string('casetype', 10)->nullable();
			$table->boolean('visible')->default(1);
			$table->boolean('fixed')->default(0);
			$table->integer('order');
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->unsignedTinyInteger('share_role')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('case_characteristic_group');
	}
}
