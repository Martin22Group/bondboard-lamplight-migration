<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenCaseTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_case', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('case_name');
			$table->mediumText('case_description');
			$table->integer('list_casecategoryid')->default(0);
			$table->integer('list_casecategory2id')->default(0);
			$table->integer('list_casecategory3id')->default(0);
			$table->integer('bodyid')->nullable();
			$table->dateTime('date_from');
			$table->dateTime('date_to')->nullable();
			$table->dateTime('date_closed')->nullable();
			$table->integer('closed_by')->nullable();
			$table->tinyInteger('open');
			$table->time('summary_time');
			$table->integer('summary_numrecords');
			$table->integer('projectid')->nullable();
			$table->tinyInteger('visible')->nullable()->default(1);
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->integer('created_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_case');
	}
}
