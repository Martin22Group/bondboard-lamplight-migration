<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenStaffWorkplanTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_staff_workplan', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('liststaffplantypeid')->index('liststaffplantypeid');
			$table->mediumText('staff_comment')->nullable();
			$table->mediumText('manager_comment')->nullable();
			$table->dateTime('target_date');
			$table->integer('achieved')->default(0);
			$table->dateTime('date_achieved')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by')->index('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_staff_workplan');
	}
}
