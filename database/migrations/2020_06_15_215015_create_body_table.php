<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBodyTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('body', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('typeuserorg')->comment('0 is a person, 1 is an org');
			$table->integer('title')->nullable();
			$table->string('first_name', 100);
			$table->string('middle_name', 100)->default('');
			$table->string('surname', 100);
			$table->integer('suffix')->nullable();
			$table->string('name')->index('name_2');
			$table->string('alternative_name');
			$table->string('metaphone_name');
			$table->boolean('typeuser')->nullable()->default(0);
			$table->boolean('typestaff')->nullable()->default(0);
			$table->boolean('typecontact')->nullable()->default(0);
			$table->boolean('typeorg')->nullable()->default(0);
			$table->boolean('typefunder')->nullable()->default(0);
			$table->integer('bodyroletype')->default(0);
			$table->boolean('visible')->nullable()->default(1);
			$table->string('phone', 30)->nullable();
			$table->string('phone2', 30)->nullable();
			$table->string('fax', 30)->nullable();
			$table->string('mobile', 30)->nullable();
			$table->string('email')->nullable();
			$table->string('web')->nullable();
			$table->string('current_address_line_1')->nullable();
			$table->string('current_address_line_2')->nullable();
			$table->string('current_address_line_3')->nullable();
			$table->string('current_address_line_4')->nullable();
			$table->string('current_address_line_5')->nullable();
			$table->string('current_postcode', 12)->nullable();
			$table->boolean('allow_email')->default(0);
			$table->boolean('allow_post')->default(0);
			$table->boolean('allow_sms')->default(0);
			$table->boolean('allow_phone')->default(0);
			$table->boolean('allow_mobile')->default(0);
			$table->text('allow_contact_notes')->nullable();
			$table->boolean('publishable')->default(0);
			$table->text('publish_summary');
			$table->boolean('publish_update')->default(0);
			$table->integer('projectid')->default(0);
			$table->boolean('allow_bulk_email')->default(1);
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->integer('project_updated')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamp('date_created')->default('0000-00-00 00:00:00');
			$table->integer('created_by');
			$table->index(['typeuser','visible'], 'typeuser');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('body');
	}
}
