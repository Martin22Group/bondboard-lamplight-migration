<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaseCharacteristicTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('case_characteristic', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('text');
			$table->integer('characteristictype')->default(1)->comment('1=select; 2=multi-select; 3=text, 4=textarea, 5=date');
			$table->boolean('required')->default(0);
			$table->integer('panel')->nullable();
			$table->boolean('historical')->default(0);
			$table->integer('record_id')->index('record_id');
			$table->integer('order');
			$table->boolean('visible');
			$table->boolean('fixed')->default(0);
			$table->text('css_style')->nullable();
			$table->text('description_text')->nullable();
			$table->text('placeholder_text')->nullable();
			$table->text('validator')->nullable();
			$table->text('default_value')->nullable();
			$table->text('javascript')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->timestamp('date_removed')->nullable();
			$table->unsignedTinyInteger('share_role')->default(0);
			$table->unsignedSmallInteger('share_rights')->default(0);
			$table->mediumText('config')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('case_characteristic');
	}
}
