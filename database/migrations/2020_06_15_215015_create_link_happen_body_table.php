<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkHappenBodyTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_happen_body', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('happenid');
			$table->integer('bodyid');
			$table->integer('bodyid2')->nullable();
			$table->integer('attendanceid');
			$table->integer('roleid');
			$table->string('notes')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->boolean('visible');
			$table->index(['bodyid','visible','happenid'], 'bodyid');
			$table->index(['bodyid2','visible','happenid'], 'bodyid2');
			$table->index(['happenid','bodyid'], 'happenid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_happen_body');
	}
}
