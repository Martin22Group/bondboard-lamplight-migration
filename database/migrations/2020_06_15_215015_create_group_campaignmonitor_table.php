<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupCampaignmonitorTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_campaignmonitor', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('group_id')->default(0)->index('group_id');
			$table->dateTime('date_sync');
			$table->text('cm_listid')->nullable();
			$table->text('cm_unsubscribepage')->nullable();
			$table->text('cm_confirmationsuccesspage')->nullable();
			$table->tinyInteger('cm_confirmedoptin')->nullable()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group_campaignmonitor');
	}
}
