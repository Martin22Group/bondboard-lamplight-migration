<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkProjectBodyTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_project_body', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('bodyid');
			$table->integer('projectid');
			$table->unsignedInteger('share_policy')->default(0);
			$table->unsignedTinyInteger('share_role')->default(0);
			$table->unsignedSmallInteger('share_rights')->default(0);
			$table->unsignedSmallInteger('share_rights_bodychar')->default(0);
			$table->unsignedSmallInteger('share_rights_happen1')->default(0);
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->index(['bodyid','projectid'], 'bodyid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_project_body');
	}
}
