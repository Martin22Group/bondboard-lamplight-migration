<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenCommTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_comm', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('comm_type');
			$table->integer('log_comm_type');
			$table->integer('draft')->nullable();
			$table->text('subject')->nullable();
			$table->text('from_name')->nullable();
			$table->text('from_email')->nullable();
			$table->text('reply_to')->nullable();
			$table->mediumText('summary_text')->nullable();
			$table->mediumText('description')->nullable();
			$table->mediumText('formData')->nullable();
			$table->text('cm_campaignid')->nullable();
			$table->text('cm_listid')->nullable();
			$table->tinyInteger('cm_status')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_comm');
	}
}
