<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkProjectHappenCharacteristicGroupTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_project_happen_characteristic_group', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('happen_characteristic_groupid');
			$table->integer('projectid');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->index(['projectid','happen_characteristic_groupid'], 'projectid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_project_happen_characteristic_group');
	}
}
