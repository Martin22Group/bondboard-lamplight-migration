<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBodyCharacteristicGroupTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('body_characteristic_group', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('groupname');
			$table->boolean('bodytype')->nullable();
			$table->boolean('typeuser');
			$table->boolean('typestaff');
			$table->boolean('typecontact');
			$table->boolean('typeorg');
			$table->boolean('typefunder');
			$table->integer('bodyroletype')->default(0);
			$table->boolean('visible')->default(1);
			$table->boolean('fixed')->default(0);
			$table->integer('order');
			$table->boolean('access_manager')->default(0);
			$table->unsignedTinyInteger('share_role')->default(0);
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('body_characteristic_group');
	}
}
