<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkProjectReporttemplateTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_project_reporttemplate', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('templateid');
			$table->integer('projectid');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_project_reporttemplate');
	}
}
