<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCostHappenTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cost_happen', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('happenid')->default(0)->index('happenid');
			$table->decimal('staff', 10)->default(0.00);
			$table->decimal('location', 10)->default(0.00);
			$table->decimal('expenses', 10)->default(0.00);
			$table->decimal('total', 10)->default(0.00);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cost_happen');
	}
}
