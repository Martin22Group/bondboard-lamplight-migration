<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportTemplateTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_template', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('template_name');
			$table->mediumText('template_description')->nullable();
			$table->tinyInteger('template_locked')->nullable()->default(0);
			$table->mediumText('form_values')->nullable();
			$table->text('form_class')->nullable();
			$table->tinyInteger('visible')->nullable()->default(1);
			$table->timestamp('date_added')->nullable();
			$table->integer('added_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('report_template');
	}
}
