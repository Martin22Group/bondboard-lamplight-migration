<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenWorkTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_work', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->mediumText('description')->nullable();
			$table->text('publish_title')->nullable();
			$table->mediumText('summary')->nullable();
			$table->mediumText('followup')->nullable();
			$table->integer('minage')->nullable()->default(0);
			$table->integer('maxage')->nullable()->default(0);
			$table->integer('publish_location')->nullable()->default(0);
			$table->text('publish_keywords')->nullable();
			$table->text('publish_category')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_work');
	}
}
