<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanningOutcomeTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planning_outcome', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('planid')->default(0);
			$table->integer('list_outcomeid')->default(0);
			$table->text('intended_outcome')->nullable();
			$table->unsignedTinyInteger('achieved')->nullable()->default(0);
			$table->text('achieved_evidence')->nullable();
			$table->boolean('visible')->nullable()->default(1);
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planning_outcome');
	}
}
