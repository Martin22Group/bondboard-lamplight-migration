<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenGrantTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_grant', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->dateTime('approval_date');
			$table->double('amount', 9, 2);
			$table->integer('grant_type');
			$table->text('description');
			$table->text('summary');
			$table->text('monitoring');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_grant');
	}
}
