<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkGroupWorkareaTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_group_workarea', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('groupid')->index('linkid');
			$table->integer('workareaid');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->index(['workareaid','groupid'], 'workareaid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_group_workarea');
	}
}
