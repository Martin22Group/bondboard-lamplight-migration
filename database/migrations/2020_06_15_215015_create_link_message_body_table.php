<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkMessageBodyTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_message_body', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('messageid')->index('messageid');
			$table->integer('bodyid')->index('bodyid');
			$table->integer('bodyid2')->nullable();
			$table->integer('typeid');
			$table->integer('how_linked')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->boolean('visible');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_message_body');
	}
}
