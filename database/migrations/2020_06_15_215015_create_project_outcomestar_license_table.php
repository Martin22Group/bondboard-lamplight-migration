<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectOutcomestarLicenseTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_outcomestar_license', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('start_date');
			$table->date('end_date')->nullable();
			$table->text('body_ids')->nullable();
			$table->text('star_ids')->nullable();
			$table->integer('max_num_operators')->nullable()->default(0);
			$table->integer('projectid')->nullable();
			$table->timestamp('date_added')->nullable();
			$table->integer('added_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_outcomestar_license');
	}
}
