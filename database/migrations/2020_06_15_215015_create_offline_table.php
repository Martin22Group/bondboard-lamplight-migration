<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfflineTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offline', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->dateTime('date_download');
			$table->integer('download_by');
			$table->integer('bodyid');
			$table->dateTime('date_upload')->nullable();
			$table->integer('upload_by')->nullable();
			$table->dateTime('date_cancelled')->nullable();
			$table->integer('cancelled_by')->nullable();
			$table->index(['bodyid','date_download'], 'bodyid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offline');
	}
}
