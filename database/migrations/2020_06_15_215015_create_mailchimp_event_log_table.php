<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailchimpEventLogTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mailchimp_event_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamp('event_date');
			$table->integer('status')->nullable();
			$table->text('title')->nullable();
			$table->mediumText('message')->nullable();
			$table->text('technical_detail')->nullable();
			$table->text('event_name')->nullable();
			$table->integer('parent_event_id')->nullable()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mailchimp_event_log');
	}
}
