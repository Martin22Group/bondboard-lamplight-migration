<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenVolunteeringOpportunityTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_volunteering_opportunity', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->mediumText('description')->nullable();
			$table->mediumText('requirements_text')->nullable();
			$table->mediumText('practical_considerations')->nullable();
			$table->integer('provider_body_id')->nullable();
			$table->mediumText('title')->nullable();
			$table->integer('location_address_id')->nullable();
			$table->integer('is_open')->nullable()->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_volunteering_opportunity');
	}
}
