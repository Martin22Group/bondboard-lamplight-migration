<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibraryItemTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('library_item', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('title')->nullable();
			$table->text('message')->nullable();
			$table->integer('categoryid')->nullable();
			$table->integer('fileid')->nullable();
			$table->text('link')->nullable();
			$table->integer('bodyid')->nullable()->index('bodyid');
			$table->dateTime('target_date')->nullable();
			$table->boolean('visible')->nullable()->default(1);
			$table->integer('added_by')->nullable();
			$table->dateTime('date_added')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('library_item');
	}
}
