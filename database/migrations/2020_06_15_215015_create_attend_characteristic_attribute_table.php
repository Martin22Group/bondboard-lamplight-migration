<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendCharacteristicAttributeTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	        
	        
		Schema::create('attend_characteristic_attribute', function(Blueprint $table)
		{
			$date = new DateTime();
			
			$table->integer('id', true);
			$table->integer('attendid');
			$table->integer('characteristicid');
			$table->integer('characteristicoptionid')->nullable();
			$table->mediumText('textvalue')->nullable();
			$table->double('numbervalue')->nullable();
			//$table->dateTime('datevalue')->nullable();
			$table->integer('attrset')->nullable()->default(0);
			$table->integer('record_number')->default(0);
			$table->boolean('visible')->default(1);
			$table->integer('projectid')->default(0);
			$table->timestamp('date_added')->useCurrent();
			//$table->integer('added_by');
			//$table->timestamp('removed_date')->default();
			//$table->integer('removed_by')->nullable();
			//$table->index(['attendid','characteristicid','visible'], 'attendid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attend_characteristic_attribute');
	}
}
