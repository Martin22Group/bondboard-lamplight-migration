<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('address', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('address_line_1')->nullable();
			$table->string('address_line_2')->nullable();
			$table->string('address_line_3')->nullable();
			$table->string('address_line_4')->nullable();
			$table->string('address_line_5')->nullable();
			$table->string('postcode', 12)->nullable();
			$table->integer('countryid')->nullable()->index('countryid');
			$table->integer('northing')->nullable();
			$table->integer('easting')->nullable();
			$table->integer('ward')->nullable();
			$table->integer('borough')->nullable();
			$table->decimal('latitude', 20, 16)->nullable();
			$table->double('longitude', 10, 6)->nullable();
			$table->string('locality')->nullable();
			$table->string('city_county')->nullable();
			$table->string('borough_district_council')->nullable();
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('address');
	}
}
