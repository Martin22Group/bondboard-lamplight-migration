<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkPlanninghighFunctionTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_planninghigh_function', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('planningid')->nullable();
			$table->integer('functionid')->nullable();
			$table->integer('proportion')->nullable();
			$table->integer('added_by')->nullable();
			$table->timestamp('date_added')->nullable()->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_planninghigh_function');
	}
}
