<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenReferralTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_referral', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('alternate_contact');
			$table->integer('referral_direction');
			$table->dateTime('appointment_date');
			$table->text('referral_reason');
			$table->text('referral_notes');
			$table->integer('referral_success');
			$table->integer('waiting_list_id');
			$table->timestamp('date_added')->default('0000-00-00 00:00:00');
			$table->integer('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_referral');
	}
}
