<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLbbRowsToRestoreTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lbb_rows_to_restore', function(Blueprint $table)
		{
			$table->integer('id')->default(0);
			$table->integer('bodyid');
			$table->integer('bodyid2');
			$table->integer('relationid')->nullable();
			$table->boolean('direction')->default(1);
			$table->boolean('default_contact')->default(0);
			$table->text('notes');
			$table->timestamp('date_from')->default('0000-00-00 00:00:00');
			$table->timestamp('date_to')->nullable();
			$table->integer('added_by');
			$table->timestamp('date_added')->default('0000-00-00 00:00:00');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lbb_rows_to_restore');
	}
}
