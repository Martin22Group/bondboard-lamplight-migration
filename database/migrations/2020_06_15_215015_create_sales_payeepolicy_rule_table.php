<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesPayeepolicyRuleTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_payeepolicy_rule', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('name');
			$table->text('ruleClass');
			$table->text('params');
			$table->unsignedTinyInteger('visible')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_payeepolicy_rule');
	}
}
