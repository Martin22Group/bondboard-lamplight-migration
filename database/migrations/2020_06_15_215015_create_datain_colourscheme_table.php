<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatainColourschemeTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('datain_colourscheme', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('name');
			$table->char('h1_color', 7);
			$table->char('h1_background_color', 7);
			$table->char('h1_border_color', 7);
			$table->char('intro_color', 7);
			$table->char('intro_background_color', 7);
			$table->char('intro_border_color', 7);
			$table->char('end_color', 7);
			$table->char('end_background_color', 7);
			$table->char('end_border_color', 7);
			$table->char('label_color', 7);
			$table->char('label_background_color', 7);
			$table->char('label_border_color', 7);
			$table->char('rowclass_odd_label_color', 7);
			$table->char('rowclass_odd_label_background_color', 7);
			$table->char('rowclass_odd_label_border_color', 7);
			$table->char('rowclass_background_color', 7);
			$table->char('rowclass_odd_background_color', 7);
			$table->char('input_select_textarea_color', 7);
			$table->char('input_select_textarea_background_color', 7);
			$table->char('input_select_textarea_border_color', 7);
			$table->char('body_background_color', 7);
			$table->char('body_color', 7);
			$table->char('page_background_color', 7);
			$table->char('form_background_color', 7);
			$table->char('form_border_color', 7);
			$table->text('custom_css');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('datain_colourscheme');
	}
}
