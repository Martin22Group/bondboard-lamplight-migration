<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkProjectOperatorTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_project_operator', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('bodyid');
			$table->integer('projectid');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->index(['bodyid','projectid'], 'bodyid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_project_operator');
	}
}
