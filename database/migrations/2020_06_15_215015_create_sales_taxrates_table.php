<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTaxratesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_taxrates', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->dateTime('date_from')->nullable();
			$table->dateTime('date_to')->nullable();
			$table->decimal('rate', 4)->default(0.00);
			$table->unsignedTinyInteger('visible')->default(1);
			$table->text('comment')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_taxrates');
	}
}
