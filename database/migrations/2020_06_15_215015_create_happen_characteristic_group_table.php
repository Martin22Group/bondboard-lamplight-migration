<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenCharacteristicGroupTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_characteristic_group', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('groupname');
			$table->string('happentype', 10)->nullable();
			$table->boolean('typework');
			$table->boolean('typeoutcome');
			$table->boolean('typecomm');
			$table->boolean('typecharge');
			$table->boolean('typeeval')->nullable()->default(0);
			$table->boolean('typereferral');
			$table->boolean('typegrant')->nullable();
			$table->boolean('typegrant_Application')->nullable();
			$table->boolean('visible')->default(1);
			$table->boolean('fixed')->default(0);
			$table->integer('order');
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->unsignedTinyInteger('share_role')->default(0);
			$table->boolean('access_manager')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_characteristic_group');
	}
}
