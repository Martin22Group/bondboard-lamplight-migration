<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublishHappenTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('publish_happen', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('happenid')->default(0);
			$table->tinyInteger('plings')->nullable()->default(0);
			$table->integer('plings_id')->nullable()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('publish_happen');
	}
}
