<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessageTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('message', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('categoryid');
			$table->string('subject');
			$table->mediumText('message');
			$table->integer('flag')->default(0);
			$table->mediumText('comment');
			$table->dateTime('display_date')->nullable()->index('display_date');
			$table->dateTime('expiry_date')->nullable();
			$table->dateTime('complete_date')->nullable();
			$table->boolean('visible')->default(1);
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->tinyInteger('system_flag')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('message');
	}
}
