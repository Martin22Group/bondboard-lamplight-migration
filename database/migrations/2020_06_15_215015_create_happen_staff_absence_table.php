<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenStaffAbsenceTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_staff_absence', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('liststaffabsenceid')->index('liststaffabsenceid');
			$table->integer('sicknote');
			$table->string('comment')->nullable();
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by')->index('added_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_staff_absence');
	}
}
