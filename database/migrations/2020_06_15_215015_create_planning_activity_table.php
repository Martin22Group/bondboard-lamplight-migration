<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanningActivityTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('planning_activity', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('date_from')->nullable();
			$table->date('date_to')->nullable();
			$table->integer('planid')->nullable();
			$table->text('activity')->nullable();
			$table->integer('list_workareaid')->nullable();
			$table->integer('list_outcomeid')->nullable();
			$table->boolean('body_type')->nullable();
			$table->text('has_happened_text')->nullable();
			$table->integer('progress')->nullable();
			$table->text('target_group')->nullable();
			$table->boolean('visible')->nullable()->default(1);
			$table->timestamp('date_added')->nullable()->useCurrent();
			$table->integer('added_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('planning_activity');
	}
}
