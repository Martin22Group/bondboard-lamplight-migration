<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatainLinkCampaignResponseTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('datain_link_campaign_response', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('bodyid');
			$table->integer('campaignid');
			$table->integer('refid');
			$table->tinyInteger('visible')->default(1);
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->index(['bodyid','campaignid'], 'bodyid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('datain_link_campaign_response');
	}
}
