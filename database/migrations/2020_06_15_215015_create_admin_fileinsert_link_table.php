<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminFileinsertLinkTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_fileinsert_link', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('bodyid')->nullable()->index('bodyid');
			$table->unsignedInteger('happenid')->nullable();
			$table->unsignedInteger('newprojectid')->nullable();
			$table->unsignedInteger('caseid')->nullable();
			$table->unsignedInteger('messageid')->nullable();
			$table->unsignedInteger('external_sourceid')->nullable();
			$table->text('externalid')->nullable();
			$table->text('projectid')->nullable();
			$table->integer('added_by')->nullable();
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_fileinsert_link');
	}
}
