<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditLogTableTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('audit_log_table', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('body_id')->index('body_id_idx');
			$table->string('subject')->nullable();
			$table->string('action')->nullable();
			$table->text('detail')->nullable();
			$table->text('summary')->nullable();
			$table->unsignedInteger('reference_id')->nullable()->default(0);
			$table->timestamp('action_date')->useCurrent();
			$table->text('request')->nullable();
			$table->unsignedInteger('project_id')->nullable()->default(0);
			$table->index(['subject','reference_id'], 'reference_idx');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('audit_log_table');
	}
}
