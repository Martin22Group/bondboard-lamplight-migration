<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVolunteeringOpportunityIdealforTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('volunteering_opportunity_idealfor', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('happenid')->index('happenid');
			$table->integer('idealfor_id');
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('volunteering_opportunity_idealfor');
	}
}
