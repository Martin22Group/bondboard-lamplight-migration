<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBodyDeduperTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('body_deduper', function(Blueprint $table)
		{
			$table->integer('id');
			$table->string('first_name', 100)->index('first_name');
			$table->string('surname', 100)->index('surname');
			$table->string('name')->index('name_2');
			$table->string('metaphone_name')->index('metaphone_name');
			$table->string('phone', 30)->nullable()->index('phone');
			$table->string('mobile', 30)->nullable()->index('mobile');
			$table->string('email')->nullable()->index('email');
			$table->string('web')->nullable()->index('web');
			$table->string('current_address_line_1')->nullable()->index('current_address_line_1');
			$table->string('current_postcode', 12)->nullable()->index('current_postcode');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('body_deduper');
	}
}
