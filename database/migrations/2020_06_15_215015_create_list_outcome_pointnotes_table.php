<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListOutcomePointnotesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('list_outcome_pointnotes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('list_outcomeid')->default(0);
			$table->integer('score')->default(0);
			$table->text('text');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('list_outcome_pointnotes');
	}
}
