<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupCampaignmonitorMembersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_campaignmonitor_members', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('groupid')->nullable()->default(0)->index('group_id');
			$table->integer('bodyid')->nullable()->default(0)->index('bodyid');
			$table->tinyInteger('action')->nullable()->default(0);
			$table->timestamp('sync')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group_campaignmonitor_members');
	}
}
