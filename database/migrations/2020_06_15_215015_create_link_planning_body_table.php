<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkPlanningBodyTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('link_planning_body', function(Blueprint $table)
		{
			$table->integer('linkid', true);
			$table->integer('planningid');
			$table->integer('bodyid');
			$table->timestamp('date_added')->useCurrent();
			$table->integer('added_by');
			$table->boolean('visible');
			$table->index(['planningid','bodyid'], 'planningid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('link_planning_body');
	}
}
