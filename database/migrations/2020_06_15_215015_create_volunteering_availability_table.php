<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVolunteeringAvailabilityTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('volunteering_availability', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('provider_body_id');
			$table->integer('happenid')->index('happenid');
			$table->tinyInteger('day_of_week')->nullable();
			$table->char('day_period', 3)->nullable();
			$table->dateTime('period_start_date')->nullable();
			$table->dateTime('period_end_date')->nullable();
			$table->integer('added_by');
			$table->timestamp('date_added')->useCurrent();
			$table->index(['day_of_week','day_period'], 'day_availabile');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('volunteering_availability');
	}
}
