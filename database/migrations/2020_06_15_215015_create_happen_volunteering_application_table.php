<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappenVolunteeringApplicationTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('happen_volunteering_application', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->mediumText('description')->nullable();
			$table->mediumText('requirements_text')->nullable();
			$table->mediumText('practical_considerations')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('happen_volunteering_application');
	}
}
