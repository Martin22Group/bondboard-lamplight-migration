# Bond Board Lamplight Migration

## Implementation
1. clone the repo
2. run ```composer install``` from the root of the project
3. copy ```.env.example``` to ```.env```
4. run the terminal instruction ```php artisan key:generate```
5. open the ```.env``` file, ensure that APP_KEY={...} is set 
6. change the database details in the ```.env``` file
        ```
	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE={your mysql database name}
	DB_USERNAME={your database username, root for example}
	DB_PASSWORD={your password}
       ```
 7. Save the file
 8. run the terminal instruction ```php artisan migrate```
 9. run the instruction ```php artisan serve```
 10. open your browser at ```http://localhost:8000``` for example 
        
## Lamplight

## API

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
